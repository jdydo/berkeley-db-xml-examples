package berkeley.example1.body;

import com.sleepycat.dbxml.XmlContainer;
import com.sleepycat.dbxml.XmlException;
import com.sleepycat.dbxml.XmlManager;

public class MainClass {
    
    public static void main(String[] args) throws Throwable {
        XmlContainer myContainer = null;
        XmlManager myManager = null;
        String containerPath = "database/container.dbxml";

        try {
            // Stworzenie XmlManager oraz stworzenie/otworzenie kontenerów:
            myManager = new XmlManager();
            myContainer = myManager.createContainer(containerPath);
            
            // Operacje
            // ...
        } catch (XmlException e) {
            // Obsługa wyjątku
            // ...
        } finally {
            try {
                // Zamykanie kontenerów orax XmlManagera:
                if (myContainer != null) {
                    myContainer.close();
                }

                if (myManager != null) {
                    myManager.close();
                }
            } catch (XmlException ce) {
                // Obsługa wyjątku
                // ...
            }
        } 
    }
}
