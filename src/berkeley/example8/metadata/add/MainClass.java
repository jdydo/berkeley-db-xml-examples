package berkeley.example8.metadata.add;

import com.sleepycat.dbxml.XmlContainer;
import com.sleepycat.dbxml.XmlException;
import com.sleepycat.dbxml.XmlManager;
import com.sleepycat.dbxml.XmlQueryContext;
import com.sleepycat.dbxml.XmlQueryExpression;
import com.sleepycat.dbxml.XmlResults;
import berkeley.setUp.SetUp;
import com.sleepycat.dbxml.XmlDocument;
import com.sleepycat.dbxml.XmlDocumentConfig;
import com.sleepycat.dbxml.XmlInputStream;
import com.sleepycat.dbxml.XmlValue;

public class MainClass {
    
    public static void main(String[] args) throws Throwable {
        // Resetowanie bazy
        SetUp.setUp();
        
        XmlContainer myContainer = null;
        XmlManager myManager = null;
        String containerPath = "database/container.dbxml";

        try {
            // Stworzenie XmlManager:
            myManager = new XmlManager();

            // Otworzenie kontenera:
            myContainer = myManager.openContainer(containerPath);
            
            // Stworzenie i skonfigurowanie XmlDocumentConfig
            XmlDocumentConfig documentConfig = new XmlDocumentConfig();

            // Otworzenie pliku XML
            XmlInputStream theStream = myManager.createLocalFileInputStream("xmlfiles/contact4.xml");

            // Stworzenie XmlDocument i wypełnienie go zawartością pliku
            XmlDocument myDoc = myManager.createDocument();
            myDoc.setContentAsXmlInputStream(theStream);
            
            // Nadanie nazwy dokumentowi
            myDoc.setName("contact004");
            
            // Stworzenie zawartości metadanej
            XmlValue attrValue = new XmlValue(XmlValue.STRING, "Aleksander");
            
            // Stworzenie metadanej i dodanie jej do dokumentu
            myDoc.setMetaData("", "createdBy", attrValue);

            // Dodawanie dokumetnu do bazy
            myContainer.putDocument(myDoc, documentConfig);  

        } catch (XmlException e) {
            System.out.println(e);
        } finally {
            try {
                // Zamykanie kontenerów orax XmlManagera:
                if (myContainer != null) {
                    myContainer.close();
                }

                if (myManager != null) {
                    myManager.close();
                }
            } catch (XmlException ce) {
                System.out.println(ce);
            }
        } 
    }
}
