package berkeley.example4.xmlContainerConfig;

import com.sleepycat.dbxml.XmlContainer;
import com.sleepycat.dbxml.XmlContainerConfig;
import com.sleepycat.dbxml.XmlException;
import com.sleepycat.dbxml.XmlManager;

public class MainClass {
    
    public static void main(String[] args) throws Throwable {
        XmlContainer myContainer = null;
        XmlManager myManager = null;
        String containerPath = "database/container.dbxml";

        try {
            // Stworzenie XmlManager:
            myManager = new XmlManager();
            
            // Stworzenie i skonfigurowanie XmlContainerConfig
            XmlContainerConfig myConfig = new XmlContainerConfig();
            myConfig.setAllowCreate(true);
            myConfig.setAllowValidation(true);
            
            // Otworzenie (stworzenie) kontenera:
            myContainer = myManager.openContainer(containerPath, myConfig);
        } catch (XmlException e) {
            System.out.println(e);
        } finally {
            try {
                // Zamykanie kontenerów orax XmlManagera:
                if (myContainer != null) {
                    myContainer.close();
                }

                if (myManager != null) {
                    myManager.close();
                }
            } catch (XmlException ce) {
                System.out.println(ce);
            }
        } 
    }
}
