package berkeley.example6.addDocument;

import com.sleepycat.dbxml.XmlContainer;
import com.sleepycat.dbxml.XmlDocumentConfig;
import com.sleepycat.dbxml.XmlException;
import com.sleepycat.dbxml.XmlInputStream;
import com.sleepycat.dbxml.XmlManager;
import berkeley.setUp.SetUp;

public class MainClass {
    
    public static void main(String[] args) throws Throwable {
        // Resetowanie bazy
        SetUp.reset();
        
        XmlContainer myContainer = null;
        XmlManager myManager = null;
        String containerPath = "database/container.dbxml";

        try {
            // Stworzenie XmlManager:
            myManager = new XmlManager();

            // Otworzenie kontenera:
            myContainer = myManager.openContainer(containerPath);
            
            // Stworzenie i skonfigurowanie XmlDocumentConfig
            XmlDocumentConfig documentConfig = new XmlDocumentConfig();
            
            // Nazwa dokumentu1
            String docName1 = "contact001";
            
            // Zawartość dokumentu1 (String)
            String doc1 = "<Contacts>\n" +
                               "    <Contact>\n" +
                               "        <ContactId>0</ContactId>\n" +
                               "        <Name>\n" +
                               "            <First>Tom</First>\n" +
                               "            <Last>Jones</Last>\n" +
                               "        </Name>\n" +
                               "        <Phone type=\"home\">420-203-2032</Phone>\n" +
                               "    </Contact>\n" +
                               "</Contacts>";

            // Nazwa dokumentu2
            String docName2 = "contact002";
            
            // Zawartość dokumentu2 (LocalFileInputStream)
            XmlInputStream doc2 = myManager.createLocalFileInputStream("xmlfiles/contact3.xml");
            
            // Dodawanie dokumetnów do bazy
            myContainer.putDocument(docName1, doc1, documentConfig);
            myContainer.putDocument(docName2, doc2, documentConfig);
            
        } catch (XmlException e) {
            System.out.println(e);
        } finally {
            try {
                // Zamykanie kontenerów orax XmlManagera:
                if (myContainer != null) {
                    myContainer.close();
                }

                if (myManager != null) {
                    myManager.close();
                }
            } catch (XmlException ce) {
                System.out.println(ce);
            }
        } 
    }
}
