package berkeley.setUp;

import com.sleepycat.dbxml.XmlContainer;
import com.sleepycat.dbxml.XmlDocument;
import com.sleepycat.dbxml.XmlDocumentConfig;
import com.sleepycat.dbxml.XmlException;
import com.sleepycat.dbxml.XmlIndexSpecification;
import com.sleepycat.dbxml.XmlInputStream;
import com.sleepycat.dbxml.XmlManager;
import com.sleepycat.dbxml.XmlValue;

public class SetUp {
    
    public static void setUp() throws Throwable {
        XmlContainer myContainer = null;
        XmlManager myManager = null;
        String containerPath = "database/container.dbxml";

        try {
            myManager = new XmlManager();
            if (myManager.existsContainer(containerPath) != 0) {
                myManager.removeContainer(containerPath);
            }
            
            myContainer = myManager.createContainer(containerPath);

            XmlIndexSpecification is = myContainer.getIndexSpecification();
            is.addIndex("", "ContactId", "unique-node-element-equality-decimal");
            myContainer.setIndexSpecification(is);
            is.delete();
            
            XmlDocumentConfig documentConfig = new XmlDocumentConfig();
            
            String docName1 = "contact001";
            String docName2 = "contact002";
            String docName3 = "contact003";
            
            XmlInputStream doc1 = myManager.createLocalFileInputStream("xmlfiles/contact1.xml");
            XmlInputStream doc2 = myManager.createLocalFileInputStream("xmlfiles/contact2.xml");
            XmlInputStream doc3 = myManager.createLocalFileInputStream("xmlfiles/contact3.xml");
            
            myContainer.putDocument(docName1, doc1, documentConfig);
            myContainer.putDocument(docName2, doc2, documentConfig);
            myContainer.putDocument(docName3, doc3, documentConfig);
            
        } catch (XmlException e) {
            System.out.println(e);
        } finally {
            try {
                if (myContainer != null) {
                    myContainer.close();
                }

                if (myManager != null) {
                    myManager.close();
                }
            } catch (XmlException ce) {
                System.out.println(ce);
            }
        } 
    }
    
    public static void reset() throws Throwable {
        XmlContainer myContainer = null;
        XmlManager myManager = null;
        String containerPath = "database/container.dbxml";

        try {
            myManager = new XmlManager();
            if (myManager.existsContainer(containerPath) != 0) {
                myManager.removeContainer(containerPath);
            }
            
            myContainer = myManager.createContainer(containerPath);

        } catch (XmlException e) {
            System.out.println(e);
        } finally {
            try {
                if (myContainer != null) {
                    myContainer.close();
                }

                if (myManager != null) {
                    myManager.close();
                }
            } catch (XmlException ce) {
                System.out.println(ce);
            }
        } 
    }
    
    public static void setUpMetadata() throws Throwable {
        SetUp.setUp();
        XmlContainer myContainer = null;
        XmlManager myManager = null;
        String containerPath = "database/container.dbxml";

        try {
            myManager = new XmlManager();
            myContainer = myManager.openContainer(containerPath);
            
            XmlDocumentConfig documentConfig = new XmlDocumentConfig();

            XmlInputStream theStream = myManager.createLocalFileInputStream("xmlfiles/contact4.xml");

            XmlDocument myDoc = myManager.createDocument();
            myDoc.setContentAsXmlInputStream(theStream);
            myDoc.setName("contact004");
            
            XmlValue attrValue = new XmlValue(XmlValue.STRING, "Aleksander");
            myDoc.setMetaData("", "createdBy", attrValue);

            myContainer.putDocument(myDoc, documentConfig);  
            
        } catch (XmlException e) {
            System.out.println(e);
        } finally {
            try {
                if (myContainer != null) {
                    myContainer.close();
                }

                if (myManager != null) {
                    myManager.close();
                }
            } catch (XmlException ce) {
                System.out.println(ce);
            }
        } 
    }
}
