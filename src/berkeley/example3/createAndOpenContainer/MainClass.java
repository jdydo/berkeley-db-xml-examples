package berkeley.example3.createAndOpenContainer;

import com.sleepycat.dbxml.XmlContainer;
import com.sleepycat.dbxml.XmlException;
import com.sleepycat.dbxml.XmlManager;

public class MainClass {
    
    public static void main(String[] args) throws Throwable {
        XmlContainer myContainer = null;
        XmlManager myManager = null;
        String containerPath = "database/container.dbxml";

        try {
            // Stworzenie XmlManager:
            myManager = new XmlManager();
            
            // Sprawdzenie czy kontener istnieje:
            int check = myManager.existsContainer(containerPath);
            if (check == 0) {
                System.out.println("Kontener nie istnieje!");
                
                // Tworzenie kontenera:
                myContainer = myManager.createContainer(containerPath);
            }
            else {
                System.out.println("Kontener istnieje!");
                
                // Otwieranie kontenera:
                myContainer = myManager.openContainer(containerPath);
            }
        } catch (XmlException e) {
            System.out.println(e);
        } finally {
            try {
                // Zamykanie kontenerów orax XmlManagera:
                if (myContainer != null) {
                    myContainer.close();
                }

                if (myManager != null) {
                    myManager.close();
                }
            } catch (XmlException ce) {
                System.out.println(ce);
            }
        } 
    }
}
