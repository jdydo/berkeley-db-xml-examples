package berkeley.example2.xmlManagerConfig;

import com.sleepycat.dbxml.XmlContainer;
import com.sleepycat.dbxml.XmlException;
import com.sleepycat.dbxml.XmlManager;
import com.sleepycat.dbxml.XmlManagerConfig;

public class MainClass {
    
    public static void main(String[] args) throws Throwable {
        XmlContainer myContainer = null;
        XmlManager myManager = null;
        String containerPath = "database/container.dbxml";

        try {
            //Stworzenie i skonfigurowanie XmlManagerConfig:
            XmlManagerConfig managerConfig = new XmlManagerConfig();
            managerConfig.setAllowAutoOpen(true);
            managerConfig.setAllowExternalAccess(true);
            
            // Stworzenie XmlManager oraz stworzenie/otworzenie kontenerów:
            myManager = new XmlManager(managerConfig);
            myContainer = myManager.createContainer(containerPath);
            
        } catch (XmlException e) {
            System.out.println(e);
        } finally {
            try {
                // Zamykanie kontenerów orax XmlManagera:
                if (myContainer != null) {
                    myContainer.close();
                }

                if (myManager != null) {
                    myManager.close();
                }
            } catch (XmlException ce) {
                System.out.println(ce);
            }
        } 
    }
}
