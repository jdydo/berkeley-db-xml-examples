package berkeley.example5.addIndex;

import berkeley.setUp.SetUp;
import com.sleepycat.dbxml.XmlContainer;
import com.sleepycat.dbxml.XmlException;
import com.sleepycat.dbxml.XmlIndexSpecification;
import com.sleepycat.dbxml.XmlManager;

public class MainClass {
    
    public static void main(String[] args) throws Throwable {
        // Resetowanie bazy
        SetUp.reset();
        
        XmlContainer myContainer = null;
        XmlManager myManager = null;
        String containerPath = "database/container.dbxml";

        try {
            // Stworzenie XmlManager:
            myManager = new XmlManager();
            
            // Otworzenie kontenera:
            myContainer = myManager.openContainer(containerPath);

            // Pobieranie indeksów z bazy
            XmlIndexSpecification is = myContainer.getIndexSpecification();

            // Dodawanie unikalnego indeksu na węźle ContactId
            is.addIndex("", "ContactId", "unique-node-element-equality-decimal");

            // Zapisywanie zmian indeksów w bazie
            myContainer.setIndexSpecification(is);
            
            // Czyszczenie obiektu XmlIndexSpecification
            is.delete();
            
        } catch (XmlException e) {
            System.out.println(e);
        } finally {
            try {
                // Zamykanie kontenerów orax XmlManagera:
                if (myContainer != null) {
                    myContainer.close();
                }

                if (myManager != null) {
                    myManager.close();
                }
            } catch (XmlException ce) {
                System.out.println(ce);
            }
        } 
    }
}
