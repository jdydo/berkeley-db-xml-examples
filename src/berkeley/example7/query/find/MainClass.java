package berkeley.example7.query.find;

import berkeley.setUp.SetUp;
import com.sleepycat.dbxml.XmlContainer;
import com.sleepycat.dbxml.XmlException;
import com.sleepycat.dbxml.XmlManager;
import com.sleepycat.dbxml.XmlQueryContext;
import com.sleepycat.dbxml.XmlQueryExpression;
import com.sleepycat.dbxml.XmlResults;

public class MainClass {
    
    public static void main(String[] args) throws Throwable {
        // Resetowanie bazy
        SetUp.setUp();
        
        XmlContainer myContainer = null;
        XmlManager myManager = null;
        String containerPath = "database/container.dbxml";

        try {
            // Stworzenie XmlManager:
            myManager = new XmlManager();

            // Otworzenie kontenera:
            myContainer = myManager.openContainer(containerPath);
            
            // Stworzenie obiektu XmlQueryContext
            XmlQueryContext context = myManager.createQueryContext();

            // Zapytanie XQuery
            String myQuery = "collection('database/container.dbxml')/Contacts/Contact[ContactId=2]";

            // Przygotowanie zapytania
            XmlQueryExpression qe = myManager.prepare(myQuery, context);

            // Wykonanie zapytania
            XmlResults results = qe.execute(context); 
            
            // Wypisywanie wyniku
            System.out.println("Ilość rekordów wynikowych: " + results.size());
            System.out.println("\nWynik:\n" + results.next().asString());
            
            // Wyczyszczenie obiektu XmlResults i XmlQueryContext
            results.delete();
            qe.delete();

        } catch (XmlException e) {
            System.out.println(e);
        } finally {
            try {
                // Zamykanie kontenerów orax XmlManagera:
                if (myContainer != null) {
                    myContainer.close();
                }

                if (myManager != null) {
                    myManager.close();
                }
            } catch (XmlException ce) {
                System.out.println(ce);
            }
        } 
    }
}
